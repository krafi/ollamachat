import tkinter as tk
from tkinter import messagebox
import re
import requests
import json
import pyttsx3
import threading

def send_message(message, model, real_time_output_enabled=True):
    # Define the endpoint URL
    url = "http://localhost:11434/api/chat"

    # Define the payload (data) to be sent in the request
    data = {
        "model": model,
        "messages": [
            {"role": "user", "content": message}
        ]
    }

    # Send the POST request to the API endpoint with streaming enabled
    response = requests.post(url, json=data, stream=True)

    # Initialize response data
    response_data = ""

    # Buffer to store assistant's responses for the current sentence
    sentence_buffer = []

    # Iterate over the streaming response
    for line in response.iter_lines():
        if line:
            # Parse the JSON line
            json_line = json.loads(line)
            # Extract the assistant's response
            assistant_response = json_line['message']['content']
            
            # Ensure the assistant's response is properly formatted
            assistant_response = assistant_response.strip()

            response_data += assistant_response + " "

            # Check if it's the final response
            if json_line.get("done"):
                break

            # Add assistant's response to the buffer
            sentence_buffer.append(assistant_response)

            # Check if the assistant's response ends with punctuation indicating the end of a sentence
            if assistant_response.endswith(('.', '!', '?')):
                # Print the buffered responses if real-time output is enabled
                if real_time_output_enabled:
                    chat_log.insert(tk.END, '\nAssistant: ' + ' '.join(sentence_buffer))
                    chat_log.see(tk.END)
                sentence_buffer = []

    return response_data.strip()


def text_to_speech(text):
    # Initialize the TTS engine
    engine = pyttsx3.init()
    # Convert text to speech
    engine.say(text)

    # Wait for the speech to finish
    engine.runAndWait()


def send_user_input(event=None):
    user_input = input_box.get()
    if user_input.lower() == 'exit':
        messagebox.showinfo("Exit", "Exiting...")
        root.destroy()
        return

    # Run the chatbot in a separate thread to prevent GUI freezing
    threading.Thread(target=process_user_input, args=(user_input,)).start()

    # Clear the input box
    input_box.delete(0, tk.END)


def process_user_input(user_input):
    # Send user's message to the server with the selected model
    assistant_response = send_message(user_input, selected_model.get(), real_time_output_enabled.get())

    # Display the assistant's response on the GUI
    chat_log.insert(tk.END, '\nYou: ' + user_input)
    chat_log.insert(tk.END, '\nAssistant: ' + assistant_response)

    # Convert the assistant's response to speech if enabled
    if text_to_speech_enabled.get():
        text_to_speech(assistant_response)


root = tk.Tk()
root.title("Ollama Chatbot")

selected_model = tk.StringVar(root, 'llama3')
text_to_speech_enabled = tk.BooleanVar(root, True)
real_time_output_enabled = tk.BooleanVar(root, True)

tk.Label(root, text="Models that you may have installed:").pack()
tk.Radiobutton(root, text="llama2-uncensored", variable=selected_model, value="llama2-uncensored").pack(anchor=tk.W)
tk.Radiobutton(root, text="llama3", variable=selected_model, value="llama3").pack(anchor=tk.W)
tk.Radiobutton(root, text="phi3", variable=selected_model, value="phi3").pack(anchor=tk.W)
tk.Radiobutton(root, text="wizardlm2", variable=selected_model, value="wizardlm2").pack(anchor=tk.W)
tk.Radiobutton(root, text="mistral", variable=selected_model, value="mistral").pack(anchor=tk.W)
tk.Radiobutton(root, text="gemma", variable=selected_model, value="gemma").pack(anchor=tk.W)
tk.Radiobutton(root, text="mixtral", variable=selected_model, value="mixtral").pack(anchor=tk.W)
tk.Radiobutton(root, text="llama2", variable=selected_model, value="llama2").pack(anchor=tk.W)

tk.Label(root, text="Do you want to enable text-to-speech?").pack()
tk.Checkbutton(root, text="Yes", variable=text_to_speech_enabled).pack(anchor=tk.W)

tk.Label(root, text="Do you want to see real-time output?").pack()
tk.Checkbutton(root, text="Yes", variable=real_time_output_enabled).pack(anchor=tk.W)

chat_log = tk.Text(root, height=20, width=50)
chat_log.pack()

input_box = tk.Entry(root, width=50)
input_box.pack()
input_box.bind("<Return>", send_user_input)

send_button = tk.Button(root, text="Send", command=send_user_input)
send_button.pack()

root.mainloop()
