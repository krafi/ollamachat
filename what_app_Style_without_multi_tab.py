import sys
import threading
from datetime import datetime
import json
from PySide6.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QTextEdit, QPushButton, QHBoxLayout, QSizePolicy
from PySide6.QtGui import QColor, QTextCursor
import ollama  # Assuming ollama is imported properly elsewhere in your environment

MODEL = 'llama3'

class ChatWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Chat Window")
        self.setGeometry(100, 100, 600, 800) 
        
        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)
        
        self.layout = QVBoxLayout()
        self.central_widget.setLayout(self.layout)
        
        self.chat_display = QTextEdit()
        self.chat_display.setReadOnly(True) 
        self.chat_display.setStyleSheet(
            "background-color: #e6e6e6; "
            "border: none; "
            "padding: 10px;"
            "border-radius: 10px;"
        )
        self.layout.addWidget(self.chat_display)
        
        self.input_layout = QHBoxLayout()
        self.layout.addLayout(self.input_layout)
        
        self.text_edit = QTextEdit()
        self.text_edit.setPlaceholderText("Type your message here...")
        self.text_edit.setStyleSheet(
            "background-color: #ffffff; "
            "border: none; "
            "padding: 10px;"
            "border-radius: 10px;"
        )
        self.text_edit.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.input_layout.addWidget(self.text_edit)
        
        self.send_button = QPushButton("Send")
        self.send_button.setStyleSheet(
            "background-color: #128C7E; "
            "color: #ffffff; "
            "border: none; "
            "padding: 10px;"
            "border-radius: 10px;"
        )
        self.send_button.clicked.connect(self.send_message)
        self.input_layout.addWidget(self.send_button)
        
        self.model = MODEL
        self.history = []

        self.user_color = QColor(0, 128, 0)  # Green
        self.assistant_color = QColor(0, 0, 255)  # Blue

    def append_message(self, role, message, color):
        formatted_message = f"<b>{role.capitalize()}:</b> {message}"
        cursor = self.chat_display.textCursor()
        cursor.movePosition(QTextCursor.End)
        cursor.insertHtml(f'<font color="{color.name()}">{formatted_message}</font><br>')
        self.chat_display.setTextCursor(cursor)

    def send_message(self):
        user_input = self.text_edit.toPlainText().strip()
        if not user_input: 
            return
        
        self.text_edit.clear()
        
        self.history.append({"role": "user", "content": user_input, "timestamp": datetime.now().strftime("%Y-%m-%d %H:%M:%S")})
        
        threading.Thread(target=self.send_and_receive_message).start()

    def send_and_receive_message(self):
        response = ollama.chat(model=self.model, messages=self.history)
        assistant_message = response['message']['content']
        assistant_response = f"Assistant: {assistant_message}"

        user_message = f"User: {self.history[-1]['content']}" 
        self.append_message("User", user_message, self.user_color)
        self.append_message("Assistant", assistant_response, self.assistant_color)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = ChatWindow()
    window.show()
    sys.exit(app.exec())
