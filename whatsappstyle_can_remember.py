import sys
import threading
from datetime import datetime
from PySide6.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QTextEdit, QPushButton, QHBoxLayout, QSizePolicy, QTabWidget
from PySide6.QtGui import QColor, QTextCursor
import ollama 

MODEL = 'llama3'

class ChatWindow(QMainWindow):
    # Define tab_histories at the class level
    tab_histories = {}

    def __init__(self):
        super().__init__()
        self.setWindowTitle("Chat Window")
        self.setGeometry(100, 100, 600, 800) 
        
        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)
        
        self.layout = QVBoxLayout()
        self.central_widget.setLayout(self.layout)

        # Add a tab widget to hold multiple tabs
        self.tab_widget = QTabWidget()
        self.layout.addWidget(self.tab_widget)
        
        # Add initial tab
        self.add_tab()  

    def add_tab(self):
        new_tab_index = self.tab_widget.count() + 1
        new_tab = QWidget()
        self.tab_widget.addTab(new_tab, f"Tab {new_tab_index}")

        layout = QVBoxLayout(new_tab)
        
        chat_display = QTextEdit()
        chat_display.setReadOnly(True) 
        chat_display.setStyleSheet(
            "background-color: #e6e6e6; "
            "border: none; "
            "padding: 10px;"
            "border-radius: 10px;"
        )
        layout.addWidget(chat_display)
        
        input_layout = QHBoxLayout()
        layout.addLayout(input_layout)
        
        text_edit = QTextEdit()
        text_edit.setPlaceholderText("Type your message here...")
        text_edit.setStyleSheet(
            "background-color: #ffffff; "
            "border: none; "
            "padding: 10px;"
            "border-radius: 10px;"
        )
        text_edit.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        input_layout.addWidget(text_edit)
        
        send_button = QPushButton("Send")
        send_button.setStyleSheet(
            "background-color: #128C7E; "
            "color: #ffffff; "
            "border: none; "
            "padding: 10px;"
            "border-radius: 10px;"
        )
        input_layout.addWidget(send_button)

        # Connect send_message to the send_button clicked signal
        send_button.clicked.connect(lambda: self.send_message(new_tab, chat_display, text_edit))

        # Initialize chat history for this tab
        self.tab_histories[new_tab] = []

        # Add "+" button next to the first tab
        if new_tab_index == 1:
            add_tab_button = QPushButton("+")
            add_tab_button.setStyleSheet(
                "background-color: #128C7E; "
                "color: #ffffff; "
                "border: none; "
                "padding: 10px;"
                "border-radius: 10px;"
            )
            add_tab_button.clicked.connect(self.add_tab)
            self.tab_widget.setCornerWidget(add_tab_button)

    def send_message(self, tab, chat_display, text_edit):
        user_input = text_edit.toPlainText().strip()
        if not user_input: 
            return
        
        text_edit.clear()
        
        # Retrieve chat history for this tab
        history = self.tab_histories[tab]
        history.append({"role": "user", "content": user_input, "timestamp": datetime.now().strftime("%Y-%m-%d %H:%M:%S")})
        
        threading.Thread(target=self.send_and_receive_message, args=(tab, chat_display, history)).start()

    def send_and_receive_message(self, tab, chat_display, history):
        response = ollama.chat(model=MODEL, messages=history)
        assistant_message = response['message']['content']
        assistant_response = f"Assistant: {assistant_message}"

        user_message = f"User: {history[-1]['content']}" 
        self.append_message(chat_display, "User", user_message, QColor(0, 128, 0))  # Green
        self.append_message(chat_display, "Assistant", assistant_response, QColor(0, 0, 255))  # Blue

    def append_message(self, chat_display, role, message, color):
        formatted_message = f"<b>{role.capitalize()}:</b> {message}"
        cursor = chat_display.textCursor()
        cursor.movePosition(QTextCursor.End)
        cursor.insertHtml(f'<font color="{color.name()}">{formatted_message}</font><br>')
        chat_display.setTextCursor(cursor)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = ChatWindow()
    window.show()
    sys.exit(app.exec())
