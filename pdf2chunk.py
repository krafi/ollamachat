import PyPDF2

def extract_text_from_pdf(pdf_path):
    text = ""
    with open(pdf_path, "rb") as file:
        reader = PyPDF2.PdfReader(file)
        for i, page in enumerate(reader.pages, start=1):
            page_text = page.extract_text()
            lines = page_text.split('\n')
            for j, line in enumerate(lines, start=1):
                text += f"Page {i}, Line {j}: {line}\n"
    return text

def split_pdf(input_pdf, output_folder, chunk_size):
    with open(input_pdf, "rb") as file:
        reader = PyPDF2.PdfReader(file)
        total_pages = len(reader.pages)

        for i in range(0, total_pages, chunk_size):
            text = ""
            for page_num in range(i, min(i + chunk_size, total_pages)):
                page = reader.pages[page_num]
                page_text = page.extract_text()
                lines = page_text.split('\n')
                for j, line in enumerate(lines, start=1):
                    text += f"Page {page_num + 1}, Line {j}: {line}\n"
            
            output_path = f"{output_folder}chunk_{i+1}-{min(i+chunk_size, total_pages)}.txt"
            with open(output_path, "w", encoding="utf-8") as output_file:
                output_file.write(text)

# Example usage
input_pdf = "input.pdf"
output_folder = "output/"
chunk_size = 10
split_pdf(input_pdf, output_folder, chunk_size)
