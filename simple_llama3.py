import ollama

MODEL = 'llama3'

def chat_with_model(user_message):
    response = ollama.chat(model=MODEL, messages=[{'role': 'user', 'content': user_message}])
    return response['message']['content']

def main():
    print("Welcome to the llama3 Chatbot!")

    while True:
        user_input = input("You: ").strip()
        if user_input.lower() == 'exit':
            print("Goodbye!")
            break
        print("AI: ", chat_with_model(user_input))

if __name__ == "__main__":
    main()
