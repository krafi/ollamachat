# Define API endpoint
$apiEndpoint = "http://localhost:11434/api/generate"

# Function to send prompt to API and receive response
function GetResponse {
    param (
        [string]$prompt
    )

    $payload = @{
        "model" = "llama3"
        "prompt" = $prompt
    } | ConvertTo-Json

    $response = Invoke-RestMethod -Uri $apiEndpoint -Method Post -Body $payload -ContentType "application/json"

    # Parse each JSON response and return the generated text
    $responseObjects = $response -split "`n" | ForEach-Object { $_ | ConvertFrom-Json }
    $generatedText = foreach ($responseObject in $responseObjects) {
        $responseObject.response
    }

    return ($generatedText -join " ")
}

# Initial prompt
Write-Output "AI: Hi there! I'm AI, nice to meet you! Is there something on your mind that you'd like to chat about or ask for help with? I'm here to listen and assist if I can."

# Main loop
while ($true) {
    # Prompt user for input
    $userInput = Read-Host "You:"

    # Send user input to API and receive response
    $responseText = GetResponse -prompt $userInput

    # Display response
    Write-Output "AI: $responseText"
}
